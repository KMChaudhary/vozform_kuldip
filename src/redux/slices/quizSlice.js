import {
    createSlice
} from "@reduxjs/toolkit";


export const quizSlice = createSlice({
    name: 'quiz',
    initialState: {
        questions: [],
        activeQuestionId: null,
        settings: {},
        theme: {},
        questionChange: false,
        settingChange: false,
        themeChange: false,
        isChanged: false
    },
    reducers: {
        initializeStore: (state, action) => {
            const {
                questions,
                theme,
                settings
            } = action.payload;
            state.questions = questions;
            state.theme = theme;
            state.settings = settings

            state.isChanged = false;
            if (questions.length > 0)
                state.activeQuestionId = questions[0].id;
        },
        addQuestion: (state, action) => {
            const newQuestion = action.payload;
            let ind = state.questions.findIndex(q => q.id === state.activeQuestionId);
            state.questions = [
                ...state.questions.slice(0, ind + 1),
                newQuestion,
                ...state.questions.slice(ind + 1)
            ]
            state.activeQuestionId = newQuestion.id;
            state.isChanged = true;
        },
        copyQuestion: (state, action) => {
            let ind = state.questions.findIndex(q => q.id === action.payload.prev_id);
            const newQuestion = action.payload.newQuestion;
            state.questions = [
                ...state.questions.slice(0, ind + 1),
                newQuestion,
                ...state.questions.slice(ind + 1)
            ]
            state.activeQuestionId = newQuestion.id;
            state.isChanged = true;
        },
        removeQuestion: (state, action) => {
            let ind = -1;
            state.questions = state.questions.filter((question, index) => {
                if (question.id === action.payload.id) {
                    if (question.name === 'welcome') return true;
                    ind = index;
                    return false;
                }
                return true;
            });

            if (ind === 0 && state.questions.length > 0) state.activeQuestionId = state.questions[ind].id;
            else if (ind > 0) state.activeQuestionId = state.questions.length > ind ? state.questions[ind].id : state.questions[ind - 1].id;
            state.isChanged = true;
        },
        reOrder: (state, action) => {
            const {
                srcIndex,
                destIndex
            } = action.payload;
            state.activeQuestionId = state.questions[srcIndex].id;
            const [removed] = state.questions.splice(srcIndex, 1);
            state.questions.splice(destIndex, 0, removed);
            state.isChanged = true;
        },
        setActiveQuestion: (state, action) => {
            state.activeQuestionId = action.payload.id;
        },
        changeAttributes: (state, action) => {
            const {
                id,
                attr_type,
                value
            } = action.payload;
            let ind = state.questions.findIndex(q => q.id === id);
            state.questions[ind].attributes[attr_type] = value;
            state.isChanged = true;
        },
        addChoice: (state, action) => {
            const {
                id,
                choiceId
            } = action.payload;
            let ind = state.questions.findIndex(q => q.id === id);
            let ch_ind = state.questions[ind].attributes.choices.length;
            state.questions[ind].attributes.choices.push({
                choiceId: choiceId,
                value: `Choice ${ch_ind+1}`,
                label: `Choice ${ch_ind+1}`,
            })
            state.isChanged = true;
        },
        removeChoice: (state, action) => {
            const {
                id,
                index
            } = action.payload;
            let q_ind = state.questions.findIndex(q => q.id === id);
            if (state.questions[q_ind].attributes.choices.length === 1) return;
            state.questions[q_ind].attributes.choices = [
                ...state.questions[q_ind].attributes.choices.slice(0, index),
                ...state.questions[q_ind].attributes.choices.slice(index + 1),
            ]
            state.isChanged = true;
        },
        modifyChoice: (state, action) => {
            const {
                id,
                index,
                label
            } = action.payload;
            let q_ind = state.questions.findIndex(q => q.id === id);
            state.questions[q_ind].attributes.choices[index].label = label;
            state.questions[q_ind].attributes.choices[index].value = label;
            state.isChanged = true;
        },
        setAttachment: (state, action) => {
            const {
                id,
                attachment
            } = action.payload;
            let q_ind = state.questions.findIndex(q => q.id === id);
            state.questions[q_ind].attributes.attachment = {
                ...attachment
            }
            state.isChanged = true;
        },
        removeAttachment: (state, action) => {
            const {
                id
            } = action.payload;
            let q_ind = state.questions.findIndex(q => q.id === id);
            state.questions[q_ind].attributes.attachment = null;
            state.isChanged = true;
        },
        saveChanges: (state, action) => {
            state.isChanged = false;
        },
        modifyTheme: (state, action) => {
            const {
                key,
                value
            } = action.payload;
            state.theme[key] = value;
            state.isChanged = true;
        }
    }
})

export const {
    initializeStore,
    addQuestion,
    changeAttributes,
    setActiveQuestion,
    addChoice,
    modifyChoice,
    removeChoice,
    removeQuestion,
    copyQuestion,
    setAttachment,
    removeAttachment,
    saveChanges,
    reOrder,
    modifyTheme
} = quizSlice.actions;

export default quizSlice.reducer;