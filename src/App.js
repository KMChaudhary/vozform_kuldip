import React from 'react';
import {Routes, Route} from 'react-router-dom';

import Links from './Links';
import VozForm from './components/VozForm/VozForm';
import Dashboard from './components/Dashboard/Dashboard';
import EditForm from './components/EditForm/EditForm';
import Result from './components/Result/Result';


function App() {
  
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Links />} />
        <Route path="/form/:form_id" element={<VozForm/>}>
          <Route index path="create" element={<EditForm />} />
          <Route path="result" element={<Result />} />          
        </Route>        
        <Route path="/dashboard" element={<Dashboard/>} />        
      </Routes>
    </div>
  );
}

export default App;
