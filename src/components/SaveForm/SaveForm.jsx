import React, {useState} from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import {saveChanges} from '../../redux/slices/quizSlice';

const SaveForm = (props) => {
    const {displayStatus} = props;
    console.log(displayStatus);
    const [isSaving, setIsSaving] = useState(false);
    const params = useParams();
    const {form_id} = params;
    const {questions, settings, theme, isChanged} = useSelector(state => state.quizReducer);
    const dispatch = useDispatch();
    const handleFormSave = () => {
        setIsSaving(true);
        fetch(`http://localhost:8000/form/${form_id}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                questions,
                settings: settings,
                theme: theme
            })
        })
        .then(res => res.json())
        .then(data => {
            setIsSaving(false);
            if(data.status === 'success') {
                console.log('data saved succeffuly');
                dispatch(saveChanges());
            }
        })
        .catch(err => {
            setIsSaving(false);
            console.log(err);
        })
    }

    const handleKeyDown = (event)=>{
        event.preventDefault();
        let charCode = String.fromCharCode(event.which).toLowerCase();
        if((event.ctrlKey || event.metaKey) && charCode === 's') {
          console.log("CTRL+S Pressed");
        }
    }

    return (
        <>
            <div className='flex items-center'>
                {
                    (displayStatus && isChanged) &&
                    <span className="text-orange-500 mr-2">Unsaved changes</span>
                }                
                {
                    (displayStatus && !isChanged) &&
                    <span className="text-slate-500 mr-2">All changes saved</span>
                }                
                <button 
                    className={`relative mr-3 px-3 py-2 flex items-center bg-blue-500 hover:bg-blue-600  font-semibold text-white rounded border`}
                    onClick={handleFormSave}
                    disabled={isSaving ? true : false}
                    onKeyDown={handleKeyDown}
                >
                    {
                        isChanged &&
                        <span class="flex h-3 w-3 absolute -top-1 -right-1">
                            <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-orange-400 opacity-75"></span>
                            <span class="relative inline-flex rounded-full h-3 w-3 bg-orange-500"></span>
                        </span>
                    }
                    
                    <span>{isSaving ? 'Saving...' : 'Save'}</span>
                </button>
            </div>            
        </>
    )
}

export default SaveForm;