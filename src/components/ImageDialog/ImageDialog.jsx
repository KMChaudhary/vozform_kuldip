import {React, useState} from 'react'
// import { Dialog, Transition } from '@headlessui/react'
import { Image, X } from 'react-bootstrap-icons'
import {useDispatch} from 'react-redux'; 
import { useParams } from 'react-router-dom';
import { setAttachment, removeAttachment } from '../../redux/slices/quizSlice';

const ImageDialog = (props) => {
    const {questionId} = props;
    const [isOpen, setIsOpen] = useState(false);

    const [image, setImage] = useState(null);
    
    const dispatch =  useDispatch();

    const params = useParams();
    const {form_id} = params;

    const handleUploadImage = async (e) => {
        const file = e.target.files[0];
        if(file.type.split("/")[0] !== "image") return;
        setImage(file);
        // upload file to server
        try {
            let imageUrl = await uploadImageAndGetUrl(file);
            console.log(imageUrl);
            dispatch(setAttachment({
                id: questionId,
                attachment: {
                    type: 'image',
                    url: imageUrl
                }
            }))
        } catch(err) {
            console.log(err);
            // Display error message
        }        
    }

    const uploadImageAndGetUrl = (file) => {
        return new Promise((resolve, reject) => {
            const formData = new FormData();
            formData.append('question_image', file);
            fetch(`http://localhost:8000/form/${form_id}/upload_img`, {
                method: 'POST',
                body: formData
            })
            .then(res => res.json())
            .then(data => {
                if(data.status === 'success')
                    resolve(data.url);
            })
        })
    }

    return (
        <>
            <button 
                className="p-2 rounded text-slate-500 hover:text-slate-700 hover:bg-slate-200 focus:ring"
                onClick={e => setIsOpen(isOpen => !isOpen)}
            >                
                <Image />
            </button>

            {
                isOpen &&
                <div className="fixed inset-0 bg-transparent flex justify-center items-center">
                    <div className="max-w-[500px] w-full rounded bg-white border shadow">
                        {/* Title */}
                        <div className="py-2 px-3 border-b">
                            <div className="flex justify-between items-center">
                                <div>Add image</div>
                                <button 
                                    className="p-1 rounded  text-slate-500 hover:text-slate-800 hover:bg-gray-100"
                                    onClick={() => setIsOpen(false)}
                                >
                                    <X size={20} />
                                </button>
                            </div>
                        </div>

                        {/* Body */}
                        <div className="p-3">
                            <div className="rounded flex justify-center items-center w-full h-[200px] bg-gray-100 border-2 border-dashed border-gray-400">
                                <label className="underline cursor-pointer" htmlFor="uploadImage">Upload</label>
                                <input 
                                    className="hidden" 
                                    type="file" 
                                    name="uploadImage" 
                                    id="uploadImage"
                                    onChange={handleUploadImage}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            }
            
        </>
    )
}

export default ImageDialog