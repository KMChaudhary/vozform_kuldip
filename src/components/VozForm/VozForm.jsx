import React from 'react';
import { ShareFill } from 'react-bootstrap-icons';
import {Outlet, NavLink, useParams, Link} from 'react-router-dom';

import FormPreview from '../FormPreview/FormPreview';
import SaveForm from '../SaveForm/SaveForm';


const Quiz = () => {
    const params = useParams();
    const {formId} = params;
    return (
        <>
            {/* Header */}
            <div className="quiz-header">
                <div className="flex h-full justify-between items-center mx-3 md:mx-5">
                    <div className="left">
                        <Link to="/dashboard" className="text-slate-400">Workspace /</Link>
                        <input 
                            className="outline-none ml-2 focus:bg-blue-100 focus:ring-1 ring-blue-500 w-fit p-1 rounded"
                            type="text"
                            name="formTitle"
                            id="formTitle"
                            defaultValue="Test form"
                        />
                    </div>
                    <div className="center">
                        <nav className="h-[56px] flex items-end">                            
                            <NavLink 
                                to={`create`} 
                                className={({isActive}) => `mx-1 inline-block py-2 px-3 border-b-4 hover:text-blue-500 ${isActive ? 'text-blue-500 font-semibold border-blue-600' : 'border-transparent'} `}
                            >
                                Create
                            </NavLink>

                            <NavLink 
                                to={`result`} 
                                className={({isActive}) => `mx-1 inline-block py-2 px-3 border-b-4 hover:text-blue-500 ${isActive ? 'text-blue-500 font-semibold border-blue-600' : 'border-transparent'}`}
                            >
                                Result
                            </NavLink>
                            
                            <NavLink 
                                to={`share`} 
                                className={({isActive}) => `mx-1 inline-block py-2 px-3 border-b-4 hover:text-blue-500 ${isActive ? 'text-blue-500 font-semibold border-blue-600' : 'border-transparent'}`}
                            >
                                Share
                            </NavLink>
                        </nav>
                    </div>
                    <div className="right">
                        <div className="flex items-center">
                            
                            <SaveForm displayStatus={true} />
                            
                            <FormPreview />

                            <button className="ml-3 px-3 py-2 flex items-center bg-green-500 hover:bg-green-600  font-semibold text-white rounded">
                                <ShareFill />
                                <span className="ml-2 font-semibold">Share</span>
                            </button>

                            <button className="w-10 h-10 ml-3 font-bold bg-blue-700 text-white flex justify-center items-center rounded-full focus:ring">
                                KC
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            {/* Main content of quiz */}
            <div className="quiz-main">
                <Outlet />
            </div>
        </>
    )
}

export default Quiz;
