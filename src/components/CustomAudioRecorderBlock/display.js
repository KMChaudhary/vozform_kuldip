import {React, useEffect} from 'react'
import { useTheme, useMessages } from "@quillforms/renderer-core";
import AudioRecorder from '../AudioRecorder/AudioRecorder';

let timer;
const RecordVoiceDisplay = (props) => {
    const {
        id,
        attributes,
        setIsValid,
        setIsAnswered,
        setValidationErr,
        isActive,
        val,
        setVal,
        next
    } = props;

    const {required} = attributes;

    const checkfieldValidation = (value) => {
        if (required === true && (!value || value === "")) {
          setIsValid(false);
          setValidationErr("Please record audio and save");
        } else {
          setIsValid(true);
          setValidationErr(null);
        }
    };

    useEffect(() => {
    if (!isActive) {
      clearTimeout(timer);
    }
    }, [isActive]);
    
    useEffect(() => {
        checkfieldValidation(val);
    }, [val]);

    const getSavedClipUrl = (err, url) => {
        if(!err) {
            console.log('url', url);
            setVal(url);

            timer = setTimeout(() => {
                setIsAnswered(true);
                next();
            }, 500);
        } else {
            clearTimeout(timer);
            setIsAnswered(false);
            setVal("");
        }
    }

    return (
        <div>
            <AudioRecorder onSave={getSavedClipUrl} />
            {/* <div>{(val === "" || !val) ? "Clip not saved" : "Clip Saved"}</div> */}
        </div>
    )
}

export default RecordVoiceDisplay