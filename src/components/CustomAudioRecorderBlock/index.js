import { registerBlockType } from "@quillforms/blocks";
import display from "./display";

registerBlockType("voice-recorder", {
  supports: {
    editable: true
  },
  attributes: {
    maxDuration: {  // number of minute responder can record audio
      type: "number",
      default: 5
    }
  },
  display
});
