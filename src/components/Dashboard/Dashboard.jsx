import React, { useEffect, useState } from 'react'
import CreateFormDialog from './CreateFormDialog'
import {Link} from 'react-router-dom'

const Dashboard = () => {
    const [forms, setForms] = useState(null);
    useEffect(() => {
        fetch('http://localhost:8000/form', {
            method: 'GET'
        })
        .then(res => res.json())
        .then(data => {
            setForms(data);
        })
        .catch(err => {
            console.log(err);
        })
    }, []);

    return (
        <>
            {/* Header */}
            <div className="header h-[56px] border-b sticky top-0">
            </div>

            {/* body */}
            <main>
                <div className="p-3">
                    <CreateFormDialog />
                    <button className="px-3 py-2 bg-slate-500 hover:bg-slate-600 focus:ring focus:ring-slate-300 text-white rounded">Create custom form</button>
                </div>

                <div className="my-4">
                    <ul>
                        {
                            forms &&
                            forms.map(form => <li key={form._id}>
                                <Link className="text-blue-500 underline px-3 py-2 block" to={`/form/${form._id}/create`}>{form.formTitle}</Link>
                            </li>)
                        }   
                    </ul>
                </div>
            </main>
        </>
    )
}

export default Dashboard