import React, { useState, useRef } from 'react'
import { useNavigate } from 'react-router-dom';
import { Dialog, Transition } from '@headlessui/react'

const CreateFormDialog = () => {
    let [isOpen, setIsOpen] = useState(false);
    let formTitleRef = useRef(null);
    let [formTitle, setFormTitle] = useState("");

    const navigate = useNavigate();

    const createNewForm = (e) => {
        e.preventDefault();
        let title = formTitle.trim() === "" ? "Untitled" : formTitle;

        fetch('http://localhost:8000/form', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({title})
        })
        .then(res => {
            return res.json();
        })
        .then(data => {
            if(data.status === "success") {
                // redirect to created form
                navigate(`/form/${data.form_id}/create`);
            }
        })
        .catch(err => {
            console.log(err);
        })
    }

    return (
        <>
            <button 
                className="mr-4 px-3 py-2 bg-blue-500 hover:bg-blue-600 focus:ring focus:ring-blue-300 text-white rounded"
                onClick={() => setIsOpen(true)}
            >Create new form</button>

            <Dialog
                open={isOpen}
                onClose={() => setIsOpen(false)}
                className="relative z-50"
                initialFocus={formTitleRef}
            >
                
                {/* The backdrop, rendered as a fixed sibling to the panel container */}
                <div className="fixed inset-0 bg-black/30" aria-hidden="true" />
                
                

                {/* Full-screen container to center the panel */}
                <div className="fixed inset-0 flex items-center justify-center p-4">

                
                    {/* The actual dialog panel  */}
                    <Dialog.Panel className="mx-auto w-full max-w-sm rounded bg-white">
                        <div className="dialog-body p-4">
                            <form action="" onSubmit={createNewForm}>
                                <div className="mb-3 flex flex-col">
                                    <label className="w-full mb-2 font-semibold" htmlFor="formTitle">Form title</label>
                                    <input 
                                        className="py-2 px-3 w-full rounded border focus:ring-1 focus:border-blue-500 focus:ring-blue-500 outline-none"
                                        type="text" 
                                        name="formTitle" 
                                        id="formTitle"
                                        ref={formTitleRef}
                                        value={formTitle}
                                        onChange={e => {setFormTitle(e.target.value)}}
                                    />
                                </div>

                                <div className="flex justify-end">
                                    <button 
                                        className="mr-4 px-3 py-2 bg-gray-200 hover:bg-gray-300 focus:ring focus:ring-gray-400 rounded"
                                        type="reset"
                                        onClick={() => {setFormTitle(""); setIsOpen(false)}}
                                    >Close</button>
                                    <button 
                                        className="px-3 py-2 bg-blue-500 hover:bg-blue-600 focus:ring focus:ring-blue-300 text-white rounded"
                                        type="submit"                                        
                                    >Create form</button>
                                </div>                                

                            </form>
                        </div>
                    </Dialog.Panel>

                </div>
                </Dialog>
        </>
    )
}

export default CreateFormDialog