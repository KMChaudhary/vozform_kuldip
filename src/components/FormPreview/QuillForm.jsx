import { useParams } from "react-router-dom";
import { Form } from "@quillforms/renderer-core";
import "@quillforms/renderer-core/build-style/style.css";
import "../CustomAudioRecorderBlock";
import { registerCoreBlocks } from "@quillforms/react-renderer-utils";
import {useSelector} from 'react-redux';

registerCoreBlocks();
const App = () => {
    const {questions, theme, settings} = useSelector(state => state.quizReducer);
    const params = useParams();
    const {form_id} = params;
    return (
        <div
            className="p-8 bg-slate-100"
            style={{ width: "100%", height: "calc(100vh - 44px)" }}>
        
        <div className="rounded-lg w-full h-full drop-shadow">
            <Form
                className="rounded-lg"
                formId="1"
                formObj={{
                blocks: questions,
                settings: settings,
                theme: theme
                }}
                onSubmit={(data, { completeForm, setIsSubmitting }) => {
                    console.log(data);
                    fetch(`http://localhost:8000/form/${form_id}/submit`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(data)
                    })
                    .then(res => res.json())
                    .then(data => console.log(data))
                    .catch(err => console.log(err.message));
                    setTimeout(() => {
                        setIsSubmitting(false);
                        completeForm();
                    }, 500);
                }}
            />
        </div>
            
        </div>
    );
};

export default App;
