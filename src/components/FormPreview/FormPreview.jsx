import { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { ArrowLeft, Eye } from 'react-bootstrap-icons'
import QuillForm from './QuillForm'


const FormPreview = (props) => {
    const [isOpen, setIsOpen] = useState(false)

    return (
        <>
            
                {
                    <button 
                        className="px-3 py-2 flex items-center bg-gray-200 hover:bg-gray-300 font-semibold rounded"
                        onClick={e => setIsOpen(true)}
                    >
                        <Eye />
                        <span className="ml-2 hidden md:block">Preview</span>
                    </button>
                }
            
                <Dialog
                open={isOpen}
                onClose={() => setIsOpen(false)}
                className="relative z-50"
                >
                
                {/* The backdrop, rendered as a fixed sibling to the panel container */}
                <div className="fixed inset-0 bg-black/30" aria-hidden="true" />
                
                

                {/* Full-screen container to center the panel */}
                <div className="fixed inset-0 flex items-center justify-center">

                
                    {/* The actual dialog panel  */}
                    <Dialog.Panel className="w-full h-full max-h-screen rounded bg-white">
                        <Dialog.Title className="border-b">
                            <div className="flex items-center w-full mx-5 md:mx-8">
                                <button 
                                    className="p-2 rounded hover:bg-slate-100"
                                    onClick={() => setIsOpen(false)}
                                ><ArrowLeft size={24} /></button>
                                <div className="flex-grow py-2 px-3 text-center text-lg font-semibold tracking-wide text-slate-700">VozForm Preview</div>
                            </div>
                        </Dialog.Title>
                        <div className="dialog-body">
                            <div className="" style={{maxHeight: 'calc(100vh - 44px)', height: 'calc(100vh - 44px)'}}>
                                {
                                    isOpen &&
                                    <QuillForm />
                                }
                            </div>
                        </div>
                    </Dialog.Panel>

                </div>
                </Dialog>           
            
        </>

        
    )
}

export default FormPreview;