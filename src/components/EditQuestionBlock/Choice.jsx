import React from 'react'
import {X} from 'react-bootstrap-icons'

const Choice = (props) => {
    const {choice, index, totalChoices, handleInputChange, handleRemoveChoice, answersColor} = props;

    return (
        <>
            <div className="mb-2">
            
                <div className="choice inline-block">
                        
                    <div 
                        style={{
                            border: `1px solid ${answersColor}`,
                            backgroundColor: answersColor+'20',
                            
                        }}
                        className="border relative border-gray-700 rounded px-2 py-2 flex bg-yellow-500 bg-opacity-10"
                    >
                        <div 
                            className="choice-index mr-3 select-none"
                            style={{
                                border: `1px solid ${answersColor}`,
                                color: answersColor
                            }}
                        >
                            {String.fromCharCode(65+index)}
                        </div>
                        <input 
                            type="text" 
                            className="choice-input" 
                            style={{
                                color: answersColor,
                                
                            }}
                            placeholder="Choice text"
                            value={choice.label}
                            onFocus={e => e.target.select()}
                            onChange={e => handleInputChange(e, index)} 
                        />
                        {
                            totalChoices > 1 &&
                            <button 
                                className="delete-choice-btn border border-white"
                                onClick={e => handleRemoveChoice(index)}
                            >
                                <X size={20} />
                            </button>
                        }
                        
                    </div>
                </div>

            </div>
        </>
    )
}

export default Choice