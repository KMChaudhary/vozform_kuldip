import {React, useRef} from 'react'
import './style.css'
import {v4 as uuidv4} from 'uuid';
import {useDispatch, useSelector} from 'react-redux';
import {changeAttributes, addChoice, removeChoice, modifyChoice, removeQuestion, copyQuestion, removeAttachment} from '../../redux/slices/quizSlice';
import {MicFill} from 'react-bootstrap-icons';

import {Files, Trash} from 'react-bootstrap-icons';

import Choice from './Choice';

import autosize from 'autosize';
import ImageDialog from '../ImageDialog/ImageDialog';

const EditQuestionBlock = (props) => {
  const {question, index} = props;
  const {theme} = useSelector(state => state.quizReducer);

  const answerTextStyle = {
    color: theme.answersColor + 'bb',
    borderBlockColor: theme.answersColor + 'bb'
  }

  const label = useRef();
  const description = useRef();

  autosize([label.current, description.current]);

  const dispatch = useDispatch();

  const handleDeleteQuestion = () => {
      dispatch(removeQuestion({
          id: question.id
      }))
  }

  const handleCopyQuestion = () => {
    const newQuestion = {
      ...question,
      id: uuidv4()
    }
    if(question.name === 'welcome') return;
    dispatch(copyQuestion({
      prev_id: question.id,
      newQuestion: newQuestion
    }))
  }

  const handleDeleteImage = () => {
    dispatch(removeAttachment({
      id: question.id
    }))
  }

  const handleLabelChange = (e) => {
      dispatch(changeAttributes({
          id: question.id,
          attr_type: 'label',
          value: e.target.value
      }))
  }
  const handleDescriptionChange = (e) => {
      dispatch(changeAttributes({
          id: question.id,
          attr_type: 'description',
          value: e.target.value
      }))
  }

  const handleAddChoice = (e) => {
      dispatch(addChoice({
          id: question.id,
          choiceId: uuidv4()
      }))
  }
  
  const handleRemoveChoice = (index) => {
      dispatch(removeChoice({
          id: question.id,
          index: index
      }))
  }

  const handleInputChange = (e, index) => {
      dispatch(modifyChoice({
          id: question.id,
          index: index,
          label: e.target.value
      }))
  }
    
    return (
      <div className="w-full h-full max-w-[900px] max-h-[570px] bg-white rounded-lg shadow border relative" style={{
        backgroundColor: theme.backgroundColor
      }}>
        <div className="flex justify-center items-center w-full h-full">
          {/* overflow style here */}
          <div className="max-w-[600px] mx-auto w-full p-8 max-h-[550px] overflow-y-auto hide-scrollbar">
            <div className={`relative ${question.name === 'welcome-screen' ? '' : 'pl-10'}`}>
              
              {
                index > 0 &&
                <div className="b-index flex items-center" style={{
                  color: theme.answersColor
                }}>{index}<span className="text-red-500 inline-block w-1 mr-1">{question.attributes.required ? '*' : ''}</span> <span className="text-lg">&rarr;</span></div>
              }

              <div>
                <textarea 
                  className={`b-label-input mb-2 ${question.name === 'welcome-screen' ? 'text-center' : ''}`}
                  style={{
                    color: theme.questionsColor
                  }}
                  name="label"
                  id="label"
                  ref={label}
                  rows={1} 
                  placeholder={question.name === 'welcome-screen' ? "Welcome screen" : "Your question here..."}
                  spellCheck={true}
                  value={question.attributes.label}
                  onChange={handleLabelChange}
                />
                <textarea 
                  className={`b-description-input ${question.name === 'welcome-screen' ? 'text-center' : ''}`}
                  style={{
                    color: theme.questionsColor
                  }}
                  name="description" 
                  id="label" 
                  ref={description}
                  rows={1} 
                  placeholder="Description (Optional)" 
                  spellCheck={true}
                  value={question.attributes.description}
                  onChange={handleDescriptionChange}
                />
              </div>

              {
                (question.attributes.attachment && question.attributes.attachment.type === 'image') &&
                <div className="my-4">
                  <div className={`max-w-[470px] relative ${question.name === 'welcome-screen' ? 'mx-auto' : ''}`}>
                    <img
                      className="w-full mx-auto"
                      src={question.attributes.attachment.url} 
                      alt="question img"
                    />
                    <div className="absolute top-0 right-0 p-2 bg-black/70">
                      <button 
                        className="delete-image text-gray-100 hover:text-red-500"
                        onClick={handleDeleteImage}
                      >
                        <Trash size={20} />
                      </button>
                    </div>
                  </div>                  
                </div>
              }
                    

              <div className="mt-6">
                  {/* WELCOME-SCREEN */}
                  {
                    question.name === 'welcome-screen' &&
                    <>
                      <div className="flex justify-center">
                        <button 
                          className="px-4 py-2 bg-slate-900 hover:bg-slate-800 focus:bg-slate-900 text-white rounded text-lg font-bold tracking-wide outline-none"
                          style={{
                            backgroundColor: theme.buttonsBgColor,
                            borderRadius: `${theme.buttonsBorderRadius}px`
                          }}
                        >
                          {question.attributes.buttonText === '' ? 'Start' : question.attributes.buttonText}
                        </button>
                      </div>
                    </>
                  }
                  {/* SORT TEXT */}
                  {
                    question.name === 'short-text' &&
                    <div className="border-b-2 text-blue-300 border-blue-300 text-xl px-3 py-2 select-none">Short text answer</div>
                  }
                  {/* LONG TEXT */}
                  {
                    question.name === 'long-text' &&
                    <div className="border-b-2 text-blue-300 border-blue-300 text-xl px-3 py-2 select-none">Long text answer</div>
                  }
                  
                  {/* NUMBER */}
                  {
                    question.name === 'number' &&
                    <div className="border-b-2 text-blue-300 border-blue-300 text-xl px-3 py-2 select-none">Number</div>
                  }
                  
                  {/* VOICE RECORDER */}
                  {
                    question.name === 'voice-recorder' &&
                    <div className="flex items-center">
                      <button 
                        className="min-w-[48px] min-h-[48px] flex justify-center items-center rounded-full select-none"
                        style={{
                          backgroundColor: theme.buttonsBgColor + '10',
                          color: theme.answersColor + 'ee',
                        }}
                      >
                        <MicFill size={20} />                        
                      </button>
                      <div 
                        className="ml-3 text-blue-300 border-blue-300 text-xl select-none"
                        style={{color: theme.answersColor + 'bb'}}
                      >
                        Record your voice response
                      </div>
                    </div>
                  }
                  

                  {/* EMAIL */}
                  {
                    question.name === 'email' &&
                    <div 
                      className="border-b-2 text-xl px-3 py-2 select-none"
                      style={answerTextStyle}
                    >name@example.com</div>
                  }

                  {/* DATE */}
                  {
                    question.name === 'date' &&
                    <div 
                      className="border-b-2 text-xl px-3 py-2 select-none" 
                      style={answerTextStyle}
                    >
                      {
                        question.attributes.format === 'DDMMYYYY' &&
                        <span>{`DD ${question.attributes.separator} MM ${question.attributes.separator} YYYY`}</span>
                      }
                      {
                        question.attributes.format === 'MMDDYYYY' &&
                        <span>{`MM ${question.attributes.separator} DD ${question.attributes.separator} YYYY`}</span>
                      }
                      {
                        question.attributes.format === 'YYYYMMDD' &&
                        <span>{`YYYY ${question.attributes.separator} MM ${question.attributes.separator} DD`}</span>
                      }
                    </div>
                  }

                  {/* WEBSITE */}
                  {
                    question.name === 'website' &&
                    <div className="border-b-2 text-blue-300 border-blue-300 text-xl px-3 py-2 select-none">https://</div>
                  }
                  
                  
                  {/* MULTIPLE CHOICE */}
                  {
                    (question.name === 'multiple-choice' || question.name === 'checkbox') &&
                    <>
                      <div className="choices flex flex-col">
                        {
                            question.attributes.choices &&
                            question.attributes.choices.map((choice, index) => <Choice key={index} index={index} choice={choice} answersColor={theme.answersColor} totalChoices={question.attributes.choices.length} handleRemoveChoice={handleRemoveChoice} handleInputChange={handleInputChange} />)
                        }        
                      </div>
                      <button 
                        className="underline text-blue-500 text-lg hover:text-blue-600 select-none"
                        style={{
                          color: theme.answersColor
                        }}
                        onClick={handleAddChoice}
                      >Add choice</button>
                    </>
                  }                 

              </div>

            </div>
          </div>
        </div>
        
        {/* TOP */}
        <div className="absolute left-0 right-0 top-0 px-4 py-4 pointer-events-none flex justify-between">
          <div>
            {/* <span>{question.name}</span> */}
          </div>

          <div className="right pointer-events-auto">
            <div className="flex items-center">
              <ImageDialog questionId={question.id} />
              {
                question.name !== 'welcome-screen' &&
                <>
                  <button 
                    className="p-2 rounded text-slate-500 hover:text-slate-700 hover:bg-slate-200 focus:ring"
                    onClick={handleCopyQuestion}
                  >
                    <Files />
                  </button>

                  <button 
                    className="p-2 rounded text-slate-500 hover:text-red-600 hover:bg-slate-200 focus:ring"
                    onClick={handleDeleteQuestion}
                  >
                    <Trash />
                  </button>
                </>
              }
              
            </div>
          </div>
          
        </div>
      </div>
    )
}

export default EditQuestionBlock;