import {v4 as uuidv4} from 'uuid';

const createQuestionObject = (type) => {
        let block = {
            id: uuidv4(),
            name: type,
            attributes: {
                label: '',
                description: '',
                required: false
            }
        }
        if(type === 'checkbox' || type === 'multiple-choice') {
            block.attributes.choices = [{
                choiceId: uuidv4(),
                value: 'Choice 1',
                label: 'Choice 1',
            }]
        }
        if(type === 'date') {
            block.attributes.format = 'DDMMYYYY';
            block.attributes.separator = '/';
        }
        
        return block;
}

export default createQuestionObject;