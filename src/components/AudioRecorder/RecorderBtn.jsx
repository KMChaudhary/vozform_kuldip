import React from 'react'

export default function RecorderBtn(props) {
    const {isRecording, handleRecorder} = props;
    console.log('is recording', isRecording);
    return (
        <div
            className="flex flex-col items-center">
            <button className="bg-blue-500 hover:bg-blue-500 p-3 rounded-lg shadow hover:shadow-lg mr-3 text-white" onClick={handleRecorder}>
            {
                isRecording ? 
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-stop-fill" viewBox="0 0 16 16">
                    <path d="M5 3.5h6A1.5 1.5 0 0 1 12.5 5v6a1.5 1.5 0 0 1-1.5 1.5H5A1.5 1.5 0 0 1 3.5 11V5A1.5 1.5 0 0 1 5 3.5z"/>
                </svg>
                :
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-mic-fill" viewBox="0 0 16 16">
                    <path d="M5 3a3 3 0 0 1 6 0v5a3 3 0 0 1-6 0V3z"/>
                    <path d="M3.5 6.5A.5.5 0 0 1 4 7v1a4 4 0 0 0 8 0V7a.5.5 0 0 1 1 0v1a5 5 0 0 1-4.5 4.975V15h3a.5.5 0 0 1 0 1h-7a.5.5 0 0 1 0-1h3v-2.025A5 5 0 0 1 3 8V7a.5.5 0 0 1 .5-.5z"/>
                </svg>                
            }    
            </button>
            <div className="mt-2">
                {isRecording ? 'Recording...' : 'Hit to record'}
            </div>
        </div>
        
    )
}
