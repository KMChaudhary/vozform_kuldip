import {useState} from 'react'

export default function useAudioRecorder() {
    const [isRecording, setIsRecording] = useState(false);
    // const [audioChunks, setAudioChunks] = useState([]);
    const [recorder, setRecorder] = useState(null);
    const [audio, setAudio] = useState(null);


    const recordAudio = () => {
        return new Promise((resolve) => {
            navigator.mediaDevices.getUserMedia({audio: true})
                .then(stream => {
                    const mediaRecorder = new MediaRecorder(stream);

                    let audioChunks = [];

                    const start = () => {
                        audioChunks = [];
                        mediaRecorder.start();
                    }

                    mediaRecorder.addEventListener('dataavailable', (e) => {
                        audioChunks.push(e.data);
                        // setAudioChunks(prevChunks => [...prevChunks, e.data]);
                    })

                    const stop = () => {
                        return new Promise(resolve => {
                            mediaRecorder.addEventListener('stop', () => {
                                const audioBlob = new Blob(audioChunks, {type : 'audio/ogg'});
                                const audioUrl = window.URL.createObjectURL(audioBlob);
                                const audio = new Audio(audioUrl);
                                
                                resolve({audioBlob, audioUrl, audio});
                            })

                            mediaRecorder.stop();
                        })
                    }

                    resolve({permission: true, start, stop});
                })
                .catch(err => {
                    console.log(err);
                });
        })
    }

    const handleRecorder = async () => {
        if(recorder == null) {
            try {
                const r = await recordAudio();
                console.log(r);
                setRecorder(r);
            }
            catch (err) {
                console.log('Audio permission not granted');
            }         
        }

        if(recorder && recorder.permission) {
            if(!isRecording) {
                recorder.start();
                setIsRecording(true);
            } else {
                const audioObj = await recorder.stop();
                setAudio(audioObj);
                setIsRecording(false);
            }
        }       
        
    }

    const deleteRecording = () => {
        setAudio(null);
    }

    const saveRecording = () => {
        return new Promise((resolve, reject) => {
            if(!audio) return;
            const formData = new FormData();
            formData.append('voice_response', audio.audioBlob);
            
            fetch('http://localhost:8000/form/upload_voiceRes', {
                method: 'POST',
                body: formData
            })
            .then(res => {
                if(res.status >= 400)
                    throw new Error("Something went wrong audio clip is not saved");
                return res.json();
            })
            .then(data => {
                console.log("Audio clip saved successfully");
                resolve({
                    url: data.url
                })
            })
            .catch(err => {
                console.log(err.message);
                reject(err);
            })
        })
        
    }

    return {
        isRecording,
        handleRecorder,
        audio,
        deleteRecording,
        saveRecording
    };
}
