import React from 'react'

export default function SaveBtn(props) {
    const {saveRecording} = props;

    return (
        <button className="bg-blue-500 hover:bg-blue-600 px-3 py-2 text-white rounded mr-2" onClick={saveRecording}>
            Save
        </button>
    )
}
