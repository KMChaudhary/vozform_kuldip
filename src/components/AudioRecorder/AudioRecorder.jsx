import React from 'react'
// import Player from './Player'
import RecorderBtn from './RecorderBtn'
import useAudioRecorder from './useAudioRecorder'
import DeleteBtn from './DeleteBtn'
import SaveBtn from './SaveBtn';

export default function AudioRecorder(props) {

    const {isRecording, handleRecorder, audio, deleteRecording, saveRecording} = useAudioRecorder();

    const {onSave} = props;

    // save file to server and return saved clip url
    const saveAndSet = async () => {
        try {
            const url = await saveRecording();
            onSave(null, url);
        } catch(err) {
            onSave(err, '');
            // onSave(null, 'abcd/xyz')
        }        
    }

    return (
        <div>
            <div className="my-8">
                <div className="flex" style={{alignItems: 'items-start'}}>
                    <RecorderBtn isRecording={isRecording} handleRecorder={handleRecorder} />

                    {
                        audio &&
                        
                            <div className="flex items-start ml-3">
                                <div className="flex items-center">
                                    <audio controls src={audio.audioUrl}></audio>
                                    
                                    <DeleteBtn deleteRecording={deleteRecording} />

                                    <SaveBtn saveRecording={saveAndSet} />
                                </div>                                
                            </div>                                                   
                    }

                </div>
            </div>
        </div>
    )
}
