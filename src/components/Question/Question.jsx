import {React, useRef} from 'react';
import {v4 as uuidv4} from 'uuid';
import {Image, Trash, Files} from 'react-bootstrap-icons';
import Choice from './Choice';
import {useDispatch} from 'react-redux';
import {changeAttributes, addChoice, removeChoice, modifyChoice, removeQuestion} from '../../redux/slices/quizSlice';
import autosize from 'autosize';

const Question = (props) => {
    const {question} = props;
    const dispatch = useDispatch();

    const handleDeleteQuestion = () => {
        dispatch(removeQuestion({
            id: question.id
        }))
    }

    const handleLabelChange = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'label',
            value: e.target.value
        }))
    }
    const handleDescriptionChange = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'description',
            value: e.target.value
        }))
    }

    // const handleRequiredChange = (e) => {
    //     dispatch(changeAttributes({
    //         id: question.id,
    //         attr_type: 'required',
    //         value: !e.target.checked
    //     }))
    // }

    const handleAddChoice = (e) => {
        dispatch(addChoice({
            id: question.id,
            choiceId: uuidv4()
        }))
    }
    
    const handleRemoveChoice = (index) => {
        dispatch(removeChoice({
            id: question.id,
            index: index
        }))
    }

    const handleInputChange = (e, index) => {
        dispatch(modifyChoice({
            id: question.id,
            index: index,
            value: e.target.value
        }))
    }

    const label = useRef();
    const description = useRef();

    autosize([label.current, description.current])

    return (
        <div className="edit-question-block relative w-full max-w-[800px] border bg-white rounded-lg drop-shadow-sm">   

            <div className="max-h-[420px] overflow-auto p-5 pb-0">
            <div className="top">
                <div className="flex items-center mb-3">
                    <textarea 
                    name="label" 
                    id="label"
                    ref={label}
                    rows={1}
                    placeholder="Write question here..."
                    className="q-label-edit"
                    value={question.label}
                    onChange={handleLabelChange}
                    />

                    <div className="relative btn-with-title ml-2">
                        <button className="w-8 h-8 flex justify-center items-center text-slate-600 hover:text-slate-700 rounded hover:bg-slate-200">
                            <Image />
                        </button>
                        <div className="hint">Question image</div>
                    </div>
                </div>

                <div className="image"></div>

                <textarea 
                name="description" 
                id="description"
                rows={1}
                ref={description}
                placeholder="Description (Optional)"
                className="q-description-edit"
                value={question.description}
                onChange={handleDescriptionChange}
                />
            </div>      

            <div className="mid">
                {
                    question.name === 'single-choice' &&
                    <div className="mt-3">
                        <div className="choices"> 
                        {
                            question.choices &&
                            question.choices.map((choice, index) => <Choice key={index} index={index} choice={choice} totalChoices={question.choices.length} handleRemoveChoice={handleRemoveChoice} handleInputChange={handleInputChange} />)
                        }
                        </div>
                        <div>
                            <button className="add-choice mb-2" onClick={handleAddChoice}>
                                Add choice
                            </button>
                        </div>
                    </div>
                }                
            </div>

            {
                question.name !== 'welcome' &&
                <div className={`solution-section my-3`}>
                    <div className="flex items-center">
                        <textarea 
                        name="solution" 
                        id="solution"
                        rows="1"
                        placeholder="Solution (Optional)"
                        className="q-solution-edit"
                        // value={question.label}
                        // onChange={handleLabelChange}
                        />

                        <button className="w-8 h-8 flex justify-center items-center ml-2 text-slate-600 hover:text-slate-700 rounded hover:bg-slate-200">
                            <Image />
                        </button>
                    </div>
                </div>
            }
            </div>

            {
                question.name !== 'welcome' &&
                <div className="tools border-t">
                    <div className="flex justify-between px-5 h-[48px] items-center">
                        <div>
                            <input type="checkbox" name="required" id="required" />                                
                            <label className="ml-2" htmlFor="required">Required</label>
                        </div>
                        <div className="flex">
                            <div className="relative btn-with-title ml-2">
                                <button className="w-8 h-8 flex justify-center items-center text-slate-600 hover:text-slate-700 rounded hover:bg-slate-200">
                                    <Files />
                                </button>
                                <div className="hint">Copy question</div>
                            </div>

                            <div className="relative btn-with-title ml-2">
                                <button 
                                className="w-8 h-8 flex justify-center items-center text-slate-600 hover:text-slate-700 hover:text-red-500 rounded hover:bg-slate-200"
                                onClick={handleDeleteQuestion}
                                >
                                    <Trash />
                                </button>
                                <div className="hint">Delete question</div>
                            </div>
                        </div>
                    </div>
                </div>
            }
            
        </div>
    )
}

export default Question