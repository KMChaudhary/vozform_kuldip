import React from 'react'
import {Circle, CheckLg, Image, Trash} from 'react-bootstrap-icons';

const Choice = (props) => {
    const {choice, index, totalChoices, handleInputChange, handleRemoveChoice} = props;

    return (
        <div className="q-choice-edit flex flex-col mb-2">
            <div className='choice-image'></div>
            <div className="flex items-center">
                <div className="mr-3 text-slate-600">
                    <Circle />
                </div>

                <input
                type="text"
                name="choice"
                value={choice.label}
                onFocus={e => e.target.select()}
                onChange={e => handleInputChange(e, index)}
                />

                <div className="relative btn-with-title choice_s-btn ml-2">
                    <button 
                    className="w-8 h-8 flex justify-center items-center text-slate-600 hover:text-slate-700 rounded hover:bg-slate-200">
                        <Image />
                    </button>
                    <div className="hint">Choice image</div>
                </div>
                
                <div className="relative btn-with-title choice_s-btn ml-2">
                    <button 
                    className="w-8 h-8 flex justify-center items-center text-slate-600 hover:text-green-500 rounded">
                        <CheckLg />
                    </button>
                    <div className="hint">Correct answer</div>
                </div>
                {
                    totalChoices > 1 &&
                    <div className="relative btn-with-title choice_s-btn ml-2">
                        <button 
                        className="w-8 h-8 flex justify-center items-center text-slate-600 hover:text-red-500"
                        onClick={e => handleRemoveChoice(index)}
                        >
                            <Trash />
                        </button>
                        <div className="hint">Delete choice</div>
                    </div>
                }
                
            </div>
            
        </div>
    )
}

export default Choice