import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import {addQuestion, setActiveQuestion, initializeStore} from '../../redux/slices/quizSlice';
import questionTypes from './questionsTypes';

import QuestionList from './QuestionList';
import QuestionSettings from './QuestionSettings';
import AddQuestionDialog from './AddQuestionDialog';
import EditQuestionBlock from '../EditQuestionBlock/EditQuestionBlock';
import createQuestionObject from '../EditQuestionBlock/createQuestion';
import { ArrowClockwise } from 'react-bootstrap-icons';
import Design from './Design';
import Settings from './Settings';


const EditForm = () => {
    const params = useParams();
    const {form_id} = params;

    const dispatch = useDispatch();
    const {questions, activeQuestionId} = useSelector(state => state.quizReducer);
    console.log(questions);

    const [isLoaded, setIsLoaded] = useState();
    const [activeMenu, setActiveMenu] = useState('question');

    useEffect(() => {
        fetch(`http://localhost:8000/form/${form_id}`)
        .then(res => res.json())
        .then(data => {
            setTimeout(() => {setIsLoaded(true)}, 500);
            dispatch(initializeStore({
                questions: data.questions,
                settings: data.settings,
                theme: data.theme,
                activeQuestionId: data.questions.length > 0 ? data.questions[0].id : null
            }))
        })
        .catch(err => {
            console.log(err);
        })
    }, [])

    // add new question
    const addNewQuestion = (type) => {
        if (!questionTypes.includes(type)) return; // if type is not there in questionsTypes then return

        let block = createQuestionObject(type);
        dispatch(addQuestion(block))
    }

    const makeCurrentQuestion = (id) => {
        dispatch(setActiveQuestion({
            id
        }));
    }

    return (
        <>
            <div className="flex h-full max-h-fixed">
                {
                    !isLoaded &&
                    <div className="flex justify-center items-center w-full h-full fixed inset-0 bg-white z-50">
                        <div className="flex items-center">
                            <span className="mr-3"><ArrowClockwise className="animate-spin" /></span>
                            <span>Loading...</span>
                        </div>
                    </div>
                }

                {/* QUESTIONS LIST */}
                <div className="lg:w-2/12 w-4/12 h-full max-h-fixed border-r bg-white">
                    {/* head */}
                    <div className="py-3 px-4 flex justify-between border-b">
                        <div className="text-slate-700 font-semibold">Questions</div>
                        <div>
                            {/* SWAP QUESTIONS BUTTON */}
                            
                            {/* ADD QUESTION DIALOG BOX */}
                            <AddQuestionDialog addNewQuestion={addNewQuestion} small={true} />
                        </div>
                    </div>

                    {/* question list */}
                    {
                        questions &&
                        <QuestionList questions={questions} activeQuestionId={activeQuestionId} makeCurrentQuestion={makeCurrentQuestion} />
                    }
                </div>

                {/* EDIT QUESTION BLOCK */}
                <div className="lg:w-8/12 w-8/12 h-full bg-gray-50">
                    <div className="px-5 h-full max-h-fixed overflow-y-auto">
                        
                        <div className="mx-auto max-w-[900px] h-full">

                            <div className="flex flex-col h-full">
                                <div className="flex-grow flex flex-col">
                                    <div className="help text-lg font-semibold tracking-wide my-3">Edit question</div>
                                    <div className="flex justify-center items-center flex-grow">

                                        {   
                                            (questions && activeQuestionId) && 
                                            <EditQuestionBlock question={questions.find(q => q.id === activeQuestionId)} index={questions.findIndex(q => q.id === activeQuestionId)} />
                                        }

                                    </div>                                
                                </div>

                                {/* add question */}
                                <div className="flex justify-end">
                                    <AddQuestionDialog addNewQuestion={addNewQuestion} small={false} />
                                </div>
                            </div>
                            
                        </div>

                    </div>
                </div>

                {/* RIGHT BAR */}
                <div className="lg:w-2/12 w-4/12 h-full max-h-fixed border-l bg-white">
                    <div className="px-2 flex justify-between items-center border-b overflow-x-auto">
                        <div 
                            className={`text-slate-700 select-none py-3 px-2 cursor-pointer hover:border-b-2 ${activeMenu === 'question' ? 'font-semibold border-b-2 border-gray-800 hover:border-gray-800' : 'hover:border-gray-400'}`}
                            onClick={e => setActiveMenu('question')}
                        >
                            Question
                        </div>

                        <div 
                            className={`text-slate-700 select-none py-3 px-2 cursor-pointer hover:border-b-2 ${activeMenu === 'design' ? 'font-semibold border-b-2 border-gray-800 hover:border-gray-800' : 'hover:border-gray-400'}`}
                            onClick={e => setActiveMenu('design')}
                        >
                            Design
                        </div>

                        <div 
                            className={`text-slate-700 select-none py-3 px-2 cursor-pointer hover:border-b-2 ${activeMenu === 'settings' ? 'font-semibold border-b-2 border-gray-800 hover:border-gray-800' : 'hover:border-gray-400'}`}
                            onClick={e => setActiveMenu('settings')}
                        >
                            Settings
                        </div>
                    </div>

                    {/* <div className="py-3 px-4 flex justify-between border-b">
                        <div className="text-slate-700 font-semibold">Settings</div>
                    </div> */}
                    {
                        (activeMenu === "question" && questions && activeQuestionId) &&
                        <QuestionSettings question={questions.find(q => q.id === activeQuestionId)} />
                    }
                    {
                        (activeMenu === "design") &&
                        <Design />
                    }
                    {
                        (activeMenu === "settings") &&
                        <Settings />
                    }
                </div>                  
            </div>
        </>
    )
}

export default EditForm;
