import React from 'react'
import {PlusLg} from 'react-bootstrap-icons'
import {useSelector, useDispatch} from 'react-redux';
import { modifyTheme } from '../../redux/slices/quizSlice';

const Design = () => {

    const {theme} = useSelector(state => state.quizReducer);
    const dispatch = useDispatch();

    const changeTheme = (key, value) => {
        dispatch(modifyTheme({key, value}));
    }

    console.log(theme.questionsColor)

    return (
        <>
            <div className="text-gray-700 sidebar-right-h-fixed overflow-y-scroll text-sm">
                {/* <div className="p-3 hover:bg-gray-100">
                    <div className="flex justify-between items-center">
                        <span className="font-semibold">Themes</span>
                        <button className="p-2 rounded bg-gray-200 hover:bg-gray-300 focus:bg-gray-200 focus:ring-1 focus:ring-gray-400">
                            <PlusLg />
                        </button>
                    </div>
                    <div className="themes">

                    </div>
                </div> */}
                
                <div className="theme">
                    <div className="p-3 text-base font-semibold text-slate-400">Design form template</div>
                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Font</label>
                            <div className="flex items-center ml-1">
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeFont" 
                                    id="themeFont"
                                    value={theme.font}                                                                       
                                />
                            </div>                            
                        </div>
                    </div>
                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Questions</label>
                            <div className="flex items-center ml-1">
                                <input 
                                    className="color-picker mr-1 outline-none" 
                                    type="color" 
                                    name="" 
                                    id=""
                                    value={theme.questionsColor}                                    
                                    onChange={(e) => changeTheme('questionsColor', e.target.value)}                       
                                />
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeFont" 
                                    id="themeFont"
                                    value={theme.questionsColor}
                                    onChange={(e) => changeTheme('questionsColor', e.target.value)}
                                />
                            </div>                            
                        </div>
                    </div>
                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Answers</label>
                            <div className="flex items-center ml-1">
                                <input 
                                    className="color-picker mr-1 outline-none" 
                                    type="color" 
                                    name="" 
                                    id="" 
                                    value={theme.answersColor}
                                    onChange={(e) => changeTheme('answersColor', e.target.value)}
                                />
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeAnswerColor" 
                                    id="themeAnswerColor"
                                    value={theme.answersColor}
                                    onChange={(e) => changeTheme('answersColor', e.target.value)}
                                />
                            </div>                            
                        </div>
                    </div>
                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Buttons</label>
                            <div className="flex items-center ml-1">
                                <input 
                                    className="color-picker mr-1 outline-none" 
                                    type="color" 
                                    name="" 
                                    id="" 
                                    value={theme.buttonsBgColor}
                                    onChange={(e) => changeTheme('buttonsBgColor', e.target.value)}
                                />
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeButtonsBgColor" 
                                    id="themeButtonsBgColor"
                                    value={theme.buttonsBgColor}
                                    onChange={(e) => changeTheme('buttonsBgColor', e.target.value)}
                                />
                            </div>                            
                        </div>
                    </div>
                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Buttons text</label>
                            <div className="flex items-center ml-1">
                                <input 
                                    className="color-picker mr-1 outline-none" 
                                    type="color" 
                                    name="" 
                                    id=""
                                    value={theme.buttonsFontColor}
                                    onChange={(e) => changeTheme('buttonsFontColor', e.target.value)} 
                                />
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeButtonsFontColor" 
                                    id="themeButtonsFontColor"
                                    value={theme.buttonsFontColor}
                                    onChange={(e) => changeTheme('buttonsFontColor', e.target.value)} 
                                />
                            </div>                            
                        </div>
                    </div>
                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Buttons border radius</label>
                            <div className="flex items-center ml-1">
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="number" 
                                    name="themeButtonsBorderRadius" 
                                    id="themeButtonsBorderRadius"
                                    value={theme.buttonsBorderRadius}
                                    min={0}
                                    max={100}
                                    step={1}
                                    onChange={(e) => changeTheme('buttonsBorderRadius', e.target.value)} 
                                />
                            </div>                            
                        </div>
                    </div>
                    
                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Errors</label>
                            <div className="flex items-center ml-1">
                                <input 
                                    className="color-picker mr-1 outline-none" 
                                    type="color" 
                                    name="" 
                                    id=""
                                    value={theme.errorsBgColor}
                                    onChange={(e) => changeTheme('errorsBgColor', e.target.value)} 
                                />
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeErrorsBgColor" 
                                    id="themeErrorsBgColor"
                                    value={theme.errorsBgColor}
                                    onChange={(e) => changeTheme('errorsBgColor', e.target.value)} 
                                />
                            </div>                            
                        </div>
                    </div>

                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Error text</label>
                            <div className="flex items-center ml-1">
                                <input 
                                    className="color-picker mr-1 outline-none" 
                                    type="color" 
                                    name="" 
                                    id=""
                                    value={theme.errorsFontColor}
                                    onChange={(e) => changeTheme('errorsFontColor', e.target.value)} 
                                />
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeFont" 
                                    id="themeFont"
                                    value={theme.errorsFontColor}
                                    onChange={(e) => changeTheme('errorsFontColor', e.target.value)} 
                                />
                            </div>                            
                        </div>
                    </div>

                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Progress bar fill color</label>
                            <div className="flex items-center ml-1">
                                <input 
                                    className="color-picker mr-1 outline-none" 
                                    type="color" 
                                    name="" 
                                    id=""
                                    value={theme.progressBarFillColor}
                                    onChange={(e) => changeTheme('progressBarFillColor', e.target.value)}                                     
                                />
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeFont" 
                                    id="themeFont"
                                    value={theme.progressBarFillColor}
                                    onChange={(e) => changeTheme('progressBarFillColor', e.target.value)}
                                />
                            </div>                            
                        </div>
                    </div>
                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Progress bar background</label>
                            <div className="flex items-center ml-1">
                                <input 
                                    className="color-picker mr-1 outline-none" 
                                    type="color" 
                                    name="" 
                                    id=""
                                    value={theme.progressBarBgColor}
                                    onChange={(e) => changeTheme('progressBarBgColor', e.target.value)}
                                />
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeFont" 
                                    id="themeFont"
                                    value={theme.progressBarBgColor}
                                    onChange={(e) => changeTheme('progressBarBgColor', e.target.value)}
                                />
                            </div>                            
                        </div>
                    </div>
                    <div className="p-3 hover:bg-gray-100">
                        <div className="flex justify-between items-center">
                            <label htmlFor="themeFont">Background</label>
                            <div className="flex items-center ml-1">
                                <input 
                                    className="color-picker mr-1 outline-none" 
                                    type="color" 
                                    name="" 
                                    id=""
                                    value={theme.backgroundColor}
                                    onChange={(e) => changeTheme('backgroundColor', e.target.value)}
                                />
                                <input
                                    className="py-1 px-2 w-[80px] border bg-transparent outline-none focus:border-gray-800 rounded"
                                    type="text" 
                                    name="themeBackground" 
                                    id="themeBackground"
                                    value={theme.backgroundColor}
                                    onChange={(e) => changeTheme('backgroundColor', e.target.value)}
                                />
                            </div>                            
                        </div>
                    </div>

                    {/* backgroundColor */}
                </div>
            </div>
        </>
    )
}

export default Design