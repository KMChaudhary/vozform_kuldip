const question_types = [
    'short-text',
    'long-text',
    'number',
    'date',
    'email',
    'website',
    'multiple-choice',
    'voice-recorder',
    'welcome-screen',
    'statement'
]


export default question_types;