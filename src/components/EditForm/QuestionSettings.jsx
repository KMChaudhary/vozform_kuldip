import React, { useState } from 'react'
import {useDispatch} from 'react-redux';
import { changeAttributes } from '../../redux/slices/quizSlice';
import { Switch } from '@headlessui/react';

const QuestionSettings = (props) => {
    const {question} = props;
    const dispatch = useDispatch();

    const handleRequiredChange = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'required',
            value: e.target.checked
        }))
    }

    const handleMultiple = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'multiple',
            value: e.target.checked
        }))
    }
    const handleVerticalAlign = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'verticalAlign',
            value: e.target.checked
        }))
    }

    const handleNumberSetMax = (e) =>{
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'set_max',
            value: e.target.checked
        }))
    }

    const handleSetMaxDuration = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'setMaxDuration',
            value: e.target.checked
        }))
    }
    
    const handleMaxDuration = (e) => {
        if(isNaN(e.target.value)) return;
        if(Number(e.target.value < 0) || Number(e.target.value > 10)) return;
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'maxDuration',
            value: e.target.value
        }))
    }

    const handleNumberMax = (e) => {
        if(isNaN(e.target.value)) return;
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'max',
            value: e.target.value
        }))
    }
    
    const handleNumberSetMin = (e) =>{
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'set_min',
            value: e.target.checked
        }))
    }

    const handleNumberMin = (e) => {
        if(isNaN(e.target.value)) return;
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'min',
            value: e.target.value
        }))
    }

    const handleSetMaxChar = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'setMaxCharacters',
            value: e.target.checked
        }))
    }
    
    const handleMaxChar = (e) => {
        if(isNaN(Number(e.target.value))) return;
        console.log('set max char', e.target.value);
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'maxCharacters',
            value: e.target.value
        }))
    }

    const handleButtonText = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'buttonText',
            value: e.target.value
        }))
    }

    const handleSeparator = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'separator',
            value: e.target.value
        }))
    }
    
    const handleFormat = (e) => {
        dispatch(changeAttributes({
            id: question.id,
            attr_type: 'format',
            value: e.target.value
        }))
    }

    return (
        <>
            <div className="text-gray-700 text-sm">
                <div className="p-3 text-base font-semibold text-slate-400">Question settings</div>
                <div className="">
                    {
                        question.name !== 'welcome-screen' &&
                        <div className="p-3 hover:bg-gray-100 flex justify-between">
                            <label htmlFor="required">Required</label>
                            <input
                                type="checkbox" 
                                name="required" 
                                id="required" 
                                checked={question.attributes.required}
                                onChange={handleRequiredChange}
                            />
                        </div>
                    }

                    {
                        question.name === 'multiple-choice' &&
                        <>
                            <div className="p-3 hover:bg-gray-100 flex justify-between">
                                <label htmlFor="multiple">Multiple selection</label>
                                <input
                                    type="checkbox" 
                                    name="multiple" 
                                    id="multiple" 
                                    checked={question.attributes.multiple}
                                    onChange={handleMultiple}
                                />
                            </div>
                            <div className="p-3 hover:bg-gray-100 flex justify-between">
                                <label htmlFor="verticalAlign">Vertical alignment</label>
                                <input
                                    type="checkbox" 
                                    name="verticalAlign" 
                                    id="verticalAlign" 
                                    checked={question.attributes.verticalAlign}
                                    onChange={handleVerticalAlign}
                                />
                            </div>
                        </>
                    }
                    
                    {
                        question.name === 'voice-recorder' &&
                        <>
                            <div className="p-3 hover:bg-gray-100 flex justify-between">
                                <label htmlFor="setMaxDuration">Set max duration</label>
                                <input
                                    type="checkbox" 
                                    name="setMaxDuration" 
                                    id="setMaxDuration" 
                                    checked={question.attributes.setMaxDuration || false}
                                    onChange={handleSetMaxDuration}
                                />
                            </div>
                            
                            {
                                question.attributes.setMaxDuration &&
                                <div className="p-3 hover:bg-gray-100 flex justify-between">
                                    <div className="mt-2 pl-3 flex items-center">
                                        <input 
                                            className="py-1 px-2 w-full border bg-transparent outline-none focus:border-gray-800 rounded" 
                                            placeholder="0-10" 
                                            type="number"
                                            min={0}
                                            max={10}
                                            name="maxDuration" 
                                            id="maxDuration" 
                                            value={question.attributes.maxDuration}
                                            onChange={handleMaxDuration}
                                        />
                                        <span className="ml-2">minute</span>
                                    </div>
                                </div>
                            }
                        </>
                    }

                    {
                        question.name === 'number' &&
                        <>
                            <div className="p-3 hover:bg-gray-100">
                                <div className="flex justify-between">
                                    <label htmlFor="setMax">Max number</label>
                                    <input 
                                        type="checkbox" 
                                        name="setMax" 
                                        id="setMax"
                                        checked={question.attributes.set_max || false}
                                        onChange={handleNumberSetMax}
                                    />
                                </div>
                                {
                                    question.attributes.set_max &&
                                    <div className="mt-2 pl-3">
                                        <input 
                                            className="py-1 px-2 w-full border bg-transparent outline-none focus:border-gray-800 rounded" 
                                            placeholder="0-9999999" 
                                            type="number" 
                                            name="max" 
                                            id="max" 
                                            value={question.attributes.max}
                                            onChange={handleNumberMax}
                                        />
                                    </div>
                                }
                            </div>
                            <div className="p-3 hover:bg-gray-100">
                                <div className="flex justify-between">
                                    <label htmlFor="setMin">Min number</label>
                                    <input 
                                        type="checkbox" 
                                        name="setMin" 
                                        id="setMin"
                                        checked={question.attributes.set_min || false}
                                        onChange={handleNumberSetMin}
                                    />
                                </div>
                                {
                                    question.attributes.set_min &&
                                    <div className="mt-2 pl-3">
                                        <input 
                                            className="py-1 px-2 w-full border bg-transparent outline-none focus:border-gray-800 rounded" 
                                            placeholder="0-9999999" 
                                            type="number" 
                                            name="min" 
                                            id="min"
                                            value={question.attributes.min}
                                            onChange={handleNumberMin}
                                        />
                                    </div>
                                }
                            </div>
                        </>
                    }                 

                    {
                        (question.name === 'short-text' || question.name === 'long-text') &&
                        <div className="p-3 hover:bg-gray-100">
                            <div className="flex justify-between">
                                <label htmlFor="setMaxCharacters">Max characters</label>
                                <input 
                                    type="checkbox" 
                                    name="setMaxCharacters" 
                                    id="setMaxCharacters"
                                    checked={question.attributes.setMaxCharacters || false}
                                    onChange={handleSetMaxChar}
                                />
                            </div>
                            {
                                question.attributes.setMaxCharacters &&
                                <div className="mt-2 pl-3">
                                    <input 
                                        className="py-1 px-2 w-full border bg-transparent outline-none focus:border-gray-800 rounded" 
                                        placeholder="0-9999999" 
                                        min={0}
                                        type="number" 
                                        name="maxCharacters" 
                                        id="maxCharacters" 
                                        value={question.attributes.maxCharacters || ''}
                                        onChange={handleMaxChar}
                                    />
                                </div>
                            }
                        </div>
                    }
                    
                    
                    {
                        question.name === 'welcome-screen' &&
                        <div className="p-3 hover:bg-gray-100">
                            <div className="mb-2">Button</div>
                            <input 
                                className="py-1 px-2 w-full border bg-transparent outline-none focus:border-gray-800 rounded" 
                                type="text" 
                                name="buttonText" 
                                id="buttonText" 
                                value={question.attributes.buttonText}
                                onChange={handleButtonText}
                            />
                        </div>
                    }
                    
                    {
                        question.name === 'date' &&
                        <div className="p-3 hover:bg-gray-100">
                            <div>Date format</div>
                            <div className="mt-2 pl-3">
                                <select value={question.attributes.format} onChange={handleFormat} className="w-full border bg-transparent outline-none focus:border-gray-800 rounded px-2 py-1">
                                    <option className="py-1 px-2" value="DDMMYYYY">DDMMYYYY</option>
                                    <option className="py-1 px-2" value="MMDDYYYY">MMDDYYYY</option>
                                    <option className="py-1 px-2" value="YYYYMMDD">YYYYMMDD</option>
                                </select>
                            </div>
                            <div className="mt-3 pl-3">
                                <select value={question.attributes.separator} onChange={handleSeparator} className="w-full border bg-transparent outline-none focus:border-gray-800 rounded px-2 py-1">
                                    <option className="py-1 px-2" value="/"> / </option>
                                    <option className="py-1 px-2" value="-"> - </option>
                                    <option className="py-1 px-2" value="."> . </option>
                                </select>
                            </div>
                        </div>
                    }
                    
                </div>
            </div>
        </>
    )
}

export default QuestionSettings