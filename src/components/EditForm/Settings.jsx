import React from 'react'

const Settings = () => {

  return (
    <>
      <div className="theme">
        <div className="p-3 text-base font-semibold text-slate-400">Form settings</div>

        <div className="p-3 hover:bg-gray-100">
          <div className="flex justify-between">
            <label htmlFor="setMaxCharacters">Schedule a close date</label>
            <input
              type="checkbox"
            />
          </div>
          {
            // question.attributes.setMaxCharacters &&
            <div className="mt-2 pl-3">
              <input
                className="py-1 px-2 w-full border bg-transparent outline-none focus:border-gray-800 rounded"
                min={0}
                type="datetime-local"
              />
            </div>
          }
        </div>

      </div>
    </>
  )
}

export default Settings