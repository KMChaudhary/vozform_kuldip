import React from 'react';
import {BodyText, Calendar2Event, Envelope, Hash, JustifyLeft, UiRadios, Link, ThreeDotsVertical, MicFill} from 'react-bootstrap-icons';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { useDispatch } from 'react-redux';
import {reOrder} from '../../redux/slices/quizSlice';

const QuestionItem = (props) => {
    const {id, name, label, index, isActive, makeCurrentQuestion} = props;
    let color;
    switch(name) {
        case 'welcome-screen':
            color= 'bg-transparent';
            break;
        case 'multiple-choice':
            color = 'bg-blue-500';
            break;
        case 'number':
            color = 'bg-indigo-500';
            break;
        case 'voice-recorder':
            color = 'bg-slate-700';
            break;
        case 'date':
            color = 'bg-slate-500';
            break;
        case 'short-text':
            color = 'bg-green-500';
            break;
        case 'long-text':
            color = 'bg-red-700';
            break;
        case 'email':
            color = 'bg-orange-500';
            break;
        case 'website':
            color = 'bg-cyan-500';
            break;  
        default:
            color = 'bg-gray-500';
            break;
    }

    return (
        <li 
        className={`py-3 px-3 text-sm cursor-pointer select-none ${isActive ? 'bg-blue-100' : 'hover:bg-slate-50'}`}
        onClick={e => makeCurrentQuestion(id)}
        >
            <div className="flex items-center">
                <div className="flex flex-grow items-center">
                    <div className={`index-icon mr-3 ${color}`}>
                        <span className="mr-2 font-semibold">{index > 0 ? index : ''}</span>
                        {
                            (name === 'welcome-screen') &&
                            <div className="text-lg">🎉</div>
                        }
                        {
                            (name === 'multiple-choice') &&
                            <UiRadios size={16} />   
                        }
                        {
                            (name === 'voice-recorder') &&
                            <MicFill size={16} />
                        }
                        {
                            (name === 'number') &&
                            <Hash size={16} />
                        }
                        {
                            (name === 'date') &&
                            <Calendar2Event size={16} />
                        }
                        {
                            (name === 'short-text') &&
                            <JustifyLeft size={16} />
                        }
                        {
                            (name === 'long-text') &&
                            <BodyText size={16} />
                        }
                        {
                            (name === 'email') &&
                            <Envelope size={16} />
                        }
                        {
                            (name === 'website') &&
                            <Link size={16} />
                        }
                    </div>
                    <div className="text-ellipsis overflow-hidden max-h-[40px] h-full">{label}</div>
                </div>
                <div className="menu">
                    <button className="ml-2 w-6 h-6 flex justify-center items-center hover:bg-slate-300 rounded">
                        <ThreeDotsVertical />
                    </button>
                </div>
            </div>
        </li>
    )
}

const QuestionList = (props) => {
    const {questions, activeQuestionId, makeCurrentQuestion} = props;
    const dispatch = useDispatch();
    const reArrangeList = (result) => {
        if(!result.destination) {
            return;
        }
        if(result.source.index === 0 || result.destination.index === 0)
            return;

        dispatch(reOrder({
            srcIndex: result.source.index,
            destIndex: result.destination.index
        }))
    }

    return (
        <>
            <DragDropContext onDragEnd={reArrangeList}>
                <Droppable droppableId="as4ad65">
                {(provided, snapshot) => (
                    <ul
                        className="h-full q-list-height overflow-auto pb-5"
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                    >
                    {questions.map((q, index) => (
                        <Draggable key={q.id} draggableId={q.id} index={index}>
                        {(provided, snapshot) => (
                            <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                style={{
                                    backgroundColor: snapshot.isDragging ? 'lightblue': '#fff',
                                    ...provided.draggableProps.style
                                }}
                            >
                                <QuestionItem
                                    id={q.id} isActive={activeQuestionId === q.id} index={index} label={q.attributes.label} name={q.name} makeCurrentQuestion={makeCurrentQuestion} 
                                />
                            </div>
                        )}
                        </Draggable>
                    ))}
                    {provided.placeholder}
                    </ul>
                )}
                </Droppable>
            </DragDropContext>
            <div className="list"></div>
        </>
    )
}

export default QuestionList;