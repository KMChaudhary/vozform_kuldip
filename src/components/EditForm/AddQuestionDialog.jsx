import { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { BodyText, Calendar2Event, Envelope, Hash, JustifyLeft, PlusLg, UiRadios, Link, MicFill } from 'react-bootstrap-icons'


const AddQuestionDialog = (props) => {
    const {addNewQuestion, small} = props;
    const [isOpen, setIsOpen] = useState(false)

    return (
        <>
            
                {
                    small &&
                    <button 
                        className="add-q-btn-sm"
                        onClick={e => setIsOpen(isOpen => !isOpen)}
                    >
                        <PlusLg />
                    </button>
                }

                {
                    !small &&
                    <button 
                        className="add-q-btn-lg my-4 flex items-center" 
                        onClick={e => setIsOpen(isOpen => !isOpen)}
                    >
                        <PlusLg />
                        <span className="ml-2">Add question</span>
                    </button>
                }
            
                <Dialog
                open={isOpen}
                onClose={() => setIsOpen(false)}
                className="relative z-50"
                >
                
                {/* The backdrop, rendered as a fixed sibling to the panel container */}
                <div className="fixed inset-0 bg-black/30" aria-hidden="true" />
                
                

                {/* Full-screen container to center the panel */}
                <div className="fixed inset-0 flex items-center justify-center p-4">

                
                    {/* The actual dialog panel  */}
                    <Dialog.Panel className="mx-auto w-full max-w-md rounded bg-white">
                        <Dialog.Title>
                            <div className="py-2 px-3 border-b text-center text-lg font-semibold tracking-wide text-slate-700">Add new question</div>
                        </Dialog.Title>
                        <div className="dialog-body">
                            <div className="flex">
                                <div className="left py-3 px-2 w-6/12">
                                    <button 
                                        className="flex items-center p-2 outline-none rounded hover:bg-slate-200 w-full"
                                        onClick={e => {
                                            addNewQuestion('multiple-choice');
                                            setIsOpen(false);
                                        }}    
                                    >
                                        <div className="p-2 bg-blue-500 text-white rounded">
                                            <UiRadios size={20} />
                                        </div>
                                        <span className="ml-3 text-left">Multiple choice</span>
                                    </button>

                                    <button 
                                        className="flex items-center p-2 outline-none rounded hover:bg-slate-200 w-full"
                                        onClick={e => {
                                            addNewQuestion('number');
                                            setIsOpen(false);
                                        }}    
                                    >
                                        <div className="p-2 bg-indigo-500 text-white rounded">
                                            <Hash size={20} />
                                        </div>
                                        <span className="ml-3 text-left">Number</span>
                                    </button>                                   

                                    <button 
                                        className="flex items-center p-2 outline-none rounded hover:bg-slate-200 w-full"
                                        onClick={e => {
                                            addNewQuestion('date');
                                            setIsOpen(false);
                                        }}    
                                    >
                                        <div className="p-2 bg-slate-500 text-white rounded">
                                            <Calendar2Event size={20} />
                                        </div>
                                        <span className="ml-3 text-left">Date</span>
                                    </button>
                                    
                                    <button 
                                        className="flex items-center p-2 outline-none rounded hover:bg-slate-200 w-full"
                                        onClick={e => {
                                            addNewQuestion('voice-recorder');
                                            setIsOpen(false);
                                        }}    
                                    >
                                        <div className="p-2 bg-slate-700 text-white rounded">
                                            <MicFill size={20} />
                                        </div>
                                        <span className="ml-3 text-left">Voice response</span>
                                    </button>


                                </div>

                                <div className="right p-3 w-6/12">
                                    <button 
                                        className="flex items-center p-2 outline-none rounded hover:bg-slate-200 w-full"
                                        onClick={e => {
                                            addNewQuestion('short-text');
                                            setIsOpen(false);
                                        }}
                                    >
                                        <div className="p-2 bg-green-500 text-white rounded">
                                            <JustifyLeft size={20} />
                                        </div>
                                        <span className="ml-3 text-left">Sort text</span>
                                    </button>

                                    <button 
                                        className="flex items-center p-2 outline-none rounded hover:bg-slate-200 w-full"
                                        onClick={e => {
                                            addNewQuestion('long-text');
                                            setIsOpen(false);
                                        }}
                                    >
                                        <div className="p-2 bg-red-700 text-white rounded">
                                            <BodyText size={20} />
                                        </div>
                                        <span className="ml-3 text-left">Long text</span>
                                    </button>
                                    <button 
                                        className="flex items-center p-2 outline-none rounded hover:bg-slate-200 w-full"
                                        onClick={e => {
                                            addNewQuestion('email');
                                            setIsOpen(false);
                                        }}
                                    >
                                        <div className="p-2 bg-orange-500 text-white rounded">
                                            <Envelope size={20} />
                                        </div>
                                        <span className="ml-3 text-left">Email</span>
                                    </button>
                                    <button 
                                        className="flex items-center p-2 outline-none rounded hover:bg-slate-200 w-full"
                                        onClick={e => {
                                            addNewQuestion('website');
                                            setIsOpen(false);
                                        }}
                                    >
                                        <div className="p-2 bg-cyan-500 text-white rounded">
                                            <Link size={20} />
                                        </div>
                                        <span className="ml-3 text-left">Website</span>
                                    </button>
                                    
                                </div>
                            </div>
                        </div>
                    </Dialog.Panel>

                </div>
                </Dialog>
            
            
        </>

        
    )
}

export default AddQuestionDialog;