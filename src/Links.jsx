import React from 'react'
import {Link} from 'react-router-dom';

const Links = () => {
  return (
    <div>
        <Link to="/quiz" className="text-blue-500 underline hover:text-blue-600">Quiz</Link>
        <br />
        <Link to="/dashboard" className="text-blue-500 underline hover:text-blue-600">Dashboard</Link>
    </div>
  )
}

export default Links;
