## form
- form_id : string (unique id)
- form_title : string
- start_datetime : datetime
- end_datetime : datetime
- questions : [question, ...] : array (array of questions objects)
- settings: Object
- theme: Object

## question
- id: string (unique id)
- name: ENUM string (question type)
- attributes
    - label : string
    - description : string
    - required :

    - setMaxCharacters
    - maxCharacters

    - setMaxDuration
    - maxDuration

    - set_min
    - min
    - set_max
    - max

    - format
    - separator

## settings
- animationDirection :  
- disableWheelSwiping
- disableNavigationArrows
- disableProgressBar

## theme
- font : string
- buttonBgColor : string ("#9b51e0")
- logo
    - src : string
- questionsColor : string
- answersColor : string
- buttonsFontColor : string
- buttonsBorderRadius : string
- errorsFontColor : string
- errorsBgColor : string
- progressBarFillColor : string
- progressBarBgColor : string
